import axios from "axios"
const consumerId = 10011
document.addEventListener('DOMContentLoaded',()=>{
    
    let _right5 = JSON.parse(localStorage.getItem('right5'))
    if(!_right5){
        _right5 = {"clientId":consumerId}
        localStorage.setItem('right5',JSON.stringify(_right5))
    }
    let pageTimer = new Date()
    setInterval(() => {
        notifyServer(new Date().getTime() - pageTimer.getTime(),JSON.parse(localStorage.getItem('right5')))
    }, 5000);
    notifyServer()

})



const notifyServer = async (timeSpent=0,right5={})=>{
    const notification = await fetch('http://localhost:8000/',{
        method:'POST',
        mode: 'cors',
        credentials:'include',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json'},
        body: JSON.stringify({"right5":right5,path:window.location,time:timeSpent})
    })
    let survey = await notification.json()
    if(survey){
        console.log(`Survey: ${survey.question}`)
        let right5 = JSON.parse(localStorage.getItem('right5'))
        if(right5['surveys']){
            right5['surveys'].push({"surveyId":survey.surveyId,"dateShown":new Date()})
        }
        else{
            console.log('NO surveys shown yet')
            right5['surveys'] = [{ "surveyId": survey.surveyId, "dateShown": new Date(),answered:false}]
            localStorage.setItem('right5',JSON.stringify(right5))
        }
    }
}