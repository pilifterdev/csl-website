export class PreciousStone {

    constructor(
        public readonly id:number,
        public name:string,
        public usage:string[],
        public colours:string[],
        public description:string,
        public url?:string,
        public planet?: string,
        public sign?:string,

        ){}
}

export function sampleStones(){
    const abalone = new PreciousStone(1,'Abalone',
        ['Psychic Awareness', 'Creativiy', 'Emotional Calm'],
        ['various'],
        'This is the peacock-hued inner shell of the Abalone mollusk. (Other mollusk species produce paler material known as Mother of Pearl.) It has a soothing effect in emotionally volatile situations, promotes psychic awareness, and enhances creative development, Its rainbow sheen harmonizes with the Root, Sacral, and Brow or Third Eye Chakra.'
    )
    const almandine = new PreciousStone(2,'Almandine',
        ['Libido', 'Health', 'Material Concerns'],
        ['red','brown'],
        'This common red to reddish-brown Garnet works with the Root Chakra to ground and resolve insecurities in matters of food, shelter, and employment. It boosts the libido and promotes health, trust, power, passion, and friendship. It can assist in the practice of Kundalini yoga.'
    )
    return [abalone, almandine]
}