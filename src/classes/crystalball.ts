import { PreciousStone, sampleStones } from "./preciousStone"


export class CrystalBall {

    constructor(public readonly id:number, public readonly description:string,public image:{},public stones?:PreciousStone[]){

    }

    toJSON(){
        return {...this}
    }
}

export function sampleCrystalWithStones(){
    const crystal = new CrystalBall(1,"a sample description",{type:'thumbnail',url:'something.png',caption:'A caption'},[...sampleStones()])
    return crystal
}
export function sampleCrystalWithoutStones(){
    const crystal = new CrystalBall(2,"another sample description",{type:'thumbnail',url:'something.png',caption:'A caption'})
    return crystal
}