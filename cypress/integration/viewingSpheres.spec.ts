import cypress from 'cypress'
describe('How the spheres page works', () => {
    it('should have spheres in the title', () => {
        cy.visit('/')
        cy.visit('/spheres')
        cy.title().should('include', "Spheres")
    })
    // it('should have a navigation', () => {
    //     cy.get('nav')
    // })
    it('should have at least one sphere to show', () => {
        cy.get('figure').should('have.length.at.least',1)
    })
    it('should be possible to go back and forward and see spheres on this page',()=>{
        cy.go('back')
        cy.title().should('equal', "Crystal Silence League")
        cy.go('forward')
        cy.get('figure').should('have.length.at.least', 1)

    })
    it('should be possible to click on any display sphere and see a modal with more information', () => {
        cy.get('figure:nth-child(5)').trigger('mouseover').click()
        cy.get('crystal-info').should('be.visible')


    })

})