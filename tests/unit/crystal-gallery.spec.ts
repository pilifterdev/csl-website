import { mount } from '@vue/test-utils'
import SphereGallery from '@/components/SphereGallery.vue'
import {sampleCrystalWithoutStones, sampleCrystalWithStones } from '~/src/classes/crystalball'

describe('Sphere Gallery', () => {

    const component = mount(SphereGallery)

  test('should be functional', () => {
    expect(component).toBeTruthy()
  })

  test('should be able to see crystal balls when added', async () => {
    await component.setProps({ crystalBalls: [sampleCrystalWithoutStones(),sampleCrystalWithStones()] })
    expect(component.props('crystalBalls')).toHaveLength(2)
    let crystalBalls = component.findAll('figure')
    expect(crystalBalls).toHaveLength(2)


  })

})
