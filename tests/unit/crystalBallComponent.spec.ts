import { mount } from '@vue/test-utils'
import CrystalBall from '@/components/CrystalBall.vue'
import { sampleCrystalWithoutStones, sampleCrystalWithStones } from '~/src/classes/crystalball'


describe('CrystalBall', () => {
  const component = mount(CrystalBall, {
    propsData: {
      crystalBall:sampleCrystalWithoutStones()
    }
  })

  test('this exists', () => {
    expect(component).toBeTruthy()
  })

  test('crystal balls should have a figure element', () => {
    const figureElement = component.find('figure')
    expect(figureElement.exists()).toBeTruthy()
  })
  test('crystal balls should have an image element', () => {
    const figureImage = component.find(' img')
    expect(figureImage.exists()).toBeTruthy()
    expect(figureImage.attributes('src')).toContain('something.png')
    expect(figureImage.attributes('alt')).toContain('A caption')
  })
  test('crystal balls should have a caption for the image', () => {
    const figureCaption = component.find('figcaption')
    expect(figureCaption.exists()).toBeTruthy()
    expect(figureCaption.text()).toContain('A caption')
  })
  test('crystal balls should have summaries of stones if available', () => {
    let stonesSummary = component.findAll('summary')
    expect(stonesSummary.exists()).toBe(false)

    const crystalBallWithStones = mount(CrystalBall, {
      propsData: {
        crystalBall: sampleCrystalWithStones()
      }}
    )
    stonesSummary = crystalBallWithStones.findAll('summary')
    expect(stonesSummary.exists()).toBeTruthy()
    expect(stonesSummary.length).toBe(2)
    expect(stonesSummary.at(1).text()).toContain('dine')
  })
  test('in gallery mode, the summaries shouldn\'t appear', () => {
    const crystalBallWithStones = mount(CrystalBall, {
      propsData: {
        crystalBall: sampleCrystalWithStones(),
        galleryMode:true
      }
    
    })
    const stonesSummary = crystalBallWithStones.findAll('summary')
    expect(stonesSummary.length).toBe(0)
  })
})
