import {PreciousStone} from '@/src/classes/preciousStone'
describe('Precious Stone', () => {
    const stone = new PreciousStone(1,'Abalone',
                 ['Psychic Awareness', 'Creativiy', 'Emotional Calm'],
                 ['various'],
                 'This is the peacock-hued inner shell of the Abalone mollusk. (Other mollusk species produce paler material known as Mother of Pearl.) It has a soothing effect in emotionally volatile situations, promotes psychic awareness, and enhances creative development, Its rainbow sheen harmonizes with the Root, Sacral, and Brow or Third Eye Chakra.'
                 )
    it('should always have the following', () => {
        expect(stone.id).toBeTruthy()
        expect(stone.name).toBeTruthy()
        expect(stone.name).toBe('Abalone')
        expect(stone.usage).toBeTruthy()
        expect(stone.colours).toBeTruthy()
        expect(stone.description).toBeTruthy()
    })
    it('should be able to add the following fields', () => {
      stone.planet = "Saturn"
      expect(stone.planet).toBe('Saturn')
      stone.sign = "Cancer"
      expect(stone.sign).toBe("Cancer")
    })  
})