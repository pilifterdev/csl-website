import {CrystalBall} from '@/src/classes/crystalball'
import {sampleStones} from '@/src/classes/preciousStone'
describe('A Crystal Ball', () => {
    const sphere = new CrystalBall(1,"sample description",{type:'thumbnail',url:'something.png',caption:'Something about them'},[])
  it('should have the following properties', () => {
    expect(sphere.id).toBeTruthy()
    expect(sphere.description).toBeTruthy()
    expect(sphere.image).toBeTruthy()
    expect(sphere.stones).toBeTruthy()
  })
  it('should be possible to add stones', () => {
    sphere.stones = sampleStones()
    expect(sphere.stones).toHaveLength(2)  
  })
})