export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    // title: 'Crystal Silence League',
    titleTemplate: (titleChunk)=>{
      return titleChunk? `${titleChunk} | Crystal Silence League`: `Crystal Silence League`
    },
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src:'~/plugins/feedback.js',mode:'client'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],
  // router:{
  //   middleware:'login'
  // },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  publicRuntimeConfig:{
    cmsURL:'${CMS_URL}',
  },
  privateRuntimeConfig:{
    clientName: '${CLIENT_ID}',
    clientPassword:'${CLIENT_PASSWORD}'
  },
  // serverMiddleware:[
  //   { path: '/api/auth', handler: '~/api/auth.js' },

  // ]
}
