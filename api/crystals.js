import express from 'express'
import cors from 'cors'
import axios from 'axios'

const app  = express()

const corsOptions = {
    origin: `${process.env.CMS_URL}`,
    optionsSuccessStatus: 200
}
app.use(cors(corsOptions))
app.use(express.json())

app.post('/', async (req, res) => {
    let { data } = await axios.post(`${process.env.CMS_URL}/collections/get/posts`, {
        token: process.env.CMS_API_KEY,
    })
    return res.json(data.entries)
})