import express from 'express'
import cors from 'cors'
import axios from 'axios'
import * as auth from '../store'
const app = express()

const corsOptions = {
    origin: `${process.env.CMS_URL}`,
    optionsSuccessStatus: 200
}
app.use(cors(corsOptions))
app.use(express.json())
app.post('/login', async (req, res) => {

    axios.post(`${process.env.CMS_URL}/auth/local`,{
        identifier: process.env.CLIENT_ID,
        password: process.env.CLIENT_PASSWORD
    })
    .then(response=>{
        if(response.data.jwt){
            return res.json(response.data.jwt)
        }
    })
    .catch(err=>{
        console.log(`Authentication fail: ${err}`)
    })

})

export default {
    path: 'api/auth',
    handler: app
}