import axios from "axios"
export const state = () => ({
    media: []
})

export const actions = {
    getMedia: async ({ state, rootState }, url) => {
        const media = (await axios.get(`${process.env.CMS_URL}/crystals-in-medias?_sort=published_at:ASC`, {
            headers: {
                'Authorization': `Bearer ${rootState.auth.accessToken}`
            }
        })).data
        state.media = [...media]
    }
}