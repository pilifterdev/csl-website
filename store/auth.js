import axios
 from "axios"
export const state = ()=>({
    accessToken:null
})

export const mutations = {
    set_token:(state,newAccessToken)=>{
        state.accessToken = newAccessToken
    },
    setCrystals:(state,setOfCrystals)=>{
    }
}
export const actions = {
    async login({commit,dispatch}){
        const {data} = await axios.post(`${process.env.CMS_URL}/auth/local`, {
            identifier: process.env.CLIENT_ID,
            password: process.env.CLIENT_PASSWORD
        })
        commit('set_token',data.jwt)
    },
    async getCrystals({commit,state},url){

        const crystals = (await axios.get(`${url}/spheres?_sort=published_at:ASC`,{
           headers:{
                'Authorization': `Bearer ${state.accessToken}`
           }
       })).data
       commit('setCrystals',crystals)
    }
}

export const getters = {
    getToken:(state)=>{
        return state.accessToken ? state.accessToken:null
    }
}