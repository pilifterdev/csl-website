import axios from "axios"
export const state = () => ({
    homePage: null
})
export const actions = {
    getHomePage: async ({ state, rootState }, url) => {
        const page = (await axios.get(`${process.env.CMS_URL}/home-page`, {
            headers: {
                'Authorization': `Bearer ${rootState.auth.accessToken}`
            }
        })).data
        state.homePage = page
    }
}