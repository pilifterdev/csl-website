import axios from "axios"
import { CrystalBall } from "~/src/classes/crystalball"
import { PreciousStone } from "~/src/classes/preciousStone"
export const state = () =>({
    crystals:[]
})

export const mutations = {
    setCrystals:(state,incomingCrystals)=>{
        incomingCrystals.forEach((crystal) => {
           const newCrystal = new CrystalBall(crystal.id,crystal.description,crystal.image,[])
           if(crystal.precious_stones){
                crystal.precious_stones.forEach((stone)=>{
                    const newStone = new PreciousStone(stone.id,stone.name,stone.usage,stone.colours,stone.description,stone.url,stone.planet,stone.sign)
                    newCrystal.stones.push(newStone)
                })
           }
           state.crystals.push(newCrystal)
       });
    }
}
export const actions = {
    getCrystals: async ({commit,rootState},url)=>{
        const crystals = (await axios.get(`${process.env.CMS_URL}/spheres?_sort=published_at:ASC`,{
           headers:{
               'Authorization':`Bearer ${rootState.auth.accessToken}`
           }
       })).data
       commit('setCrystals',crystals)
    }
}
export const getters ={
    numberOfCrystals:(state)=>{
        return state.crystals.length
    },
    hasCrystals: (state) => {
        return state.crystals.length > 0
    }
}