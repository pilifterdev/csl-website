export const actions = {
    async nuxtServerInit({dispatch,state}){
        if(!state.auth.accessToken){
            await dispatch('auth/login')
        }
        else{
            console.log("It's cool. got a token")
        }
        
        if(!state.crystals.crystals.length){
            await dispatch('crystals/getCrystals')
        }
        if(!state.media.media.length){
            await dispatch('media/getMedia')
        }
        if(!state.homePage.homePage){
            await dispatch('homePage/getHomePage')
        }
    }
}

